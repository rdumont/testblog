# This article tests code

Testing syntax highlighting in many ways.

## Hulog mode

<pre lang=csharp>
public class Foo
{
    public void Bar()
    {
    }
}
</pre>

## GitHub mode

```csharp
public class Foo
{
    public void Bar()
    {
    }
}
```

## Bitbucket mode

    :::csharp
    public class Foo
    {
        public void Bar()
        {
        }
    }

## This is a table

Make | Model | Year
-----|-------|-----
Audi | TT    | 2011
Fiat | Palio | 2013